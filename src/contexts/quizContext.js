import Loading from "components/Loading";
import React, { createContext, useState, useEffect } from "react";
import { quizAPI } from "services/quizAPI";

export const QuizContext = createContext();

const QuizContextProvider = ({ children }) => {
  const [questions, setQuestions] = useState([]);
  const [currentQuestion, setCurrentQuestion] = useState(0);
  const [userAnswers, setUserAnswers] = useState([]);
  const [correctAnswers, setCorrectAnswers] = useState(0);
  const [showReviewAnswers, setShowReviewAnswers] = useState(false);
  const [isQuizStarted, setIsQuizStarted] = useState(false);
  const [isQuizFinished, setIsQuizFinished] = useState(false);
  const [isLoading, setIsLoading] = useState(true);
  const [startTime, setStartTime] = useState(null);
  const [endTime, setEndTime] = useState(null);

  const handleAnswer = (selectedAnswer) => {
    const currentAnswer = questions?.[currentQuestion]?.correct_answer;
    const isCorrect = selectedAnswer === currentAnswer;
    if (isCorrect) {
      setCorrectAnswers((prevCorrectAnswers) => prevCorrectAnswers + 1);
    }
    setUserAnswers((prevUserAnswers) => [...prevUserAnswers, selectedAnswer]);
    if (currentQuestion === questions?.length - 1) {
      setIsQuizFinished(true);
      setEndTime(new Date());
    }
  };

  const handleNextQuestion = () => {
    setCurrentQuestion((prevQuestion) => prevQuestion + 1);
  };

  const handlePlayAgain = () => {
    setCurrentQuestion(0);
    setUserAnswers([]);
    setCorrectAnswers(0);
    setShowReviewAnswers(false);
    setIsQuizFinished(false);
    setStartTime(new Date());
    setEndTime(null);
  };

  const handleReviewAnswers = () => {
    setShowReviewAnswers(true);
  };

  const handleStartQuiz = () => {
    setIsQuizStarted(true);
    setStartTime(new Date());
  };

  useEffect(() => {
    const fetchQuestions = async (amount) => {
      try {
        const response = await quizAPI.getResults((amount = 10));
        setQuestions(response.data.results);
        setIsLoading(false);
      } catch (error) {
        alert(error);
        setIsLoading(false);
      }
    };

    fetchQuestions();
  }, []);

  const calculateTimeTaken = () => {
    if (startTime && endTime) {
      const timeDifference = endTime - startTime;
      const seconds = Math.floor(timeDifference / 1000);
      const minutes = Math.floor(seconds / 60);
      const formattedTime = `${minutes}m ${seconds % 60}s`;
      return formattedTime;
    }
    return "N/A";
  };

  const calculatePassingGrade = () => {
    const passingPercentage = 70;
    const percentage = (correctAnswers / questions?.length) * 100;
    return percentage >= passingPercentage;
  };

  const score = Math.floor((correctAnswers / questions?.length) * 100);
  const timeTaken = calculateTimeTaken();
  const isPassed = calculatePassingGrade();
  const resultText = isPassed
    ? "Congratulations! You passed the quiz."
    : "Oops! You failed the quiz.";

  if (isLoading) {
    return <Loading />;
  }

  return (
    <QuizContext.Provider
      value={{
        questions,
        currentQuestion,
        userAnswers,
        correctAnswers,
        handleAnswer,
        handleNextQuestion,
        handlePlayAgain,
        handleReviewAnswers,
        showReviewAnswers,
        handleStartQuiz,
        isQuizStarted,
        isQuizFinished,
        score,
        timeTaken,
        resultText,
      }}
    >
      {children}
    </QuizContext.Provider>
  );
};

export default QuizContextProvider;
