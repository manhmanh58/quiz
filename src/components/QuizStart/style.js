import styled from "styled-components";

const StartFormContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  height: 100vh;
`;

const StartFormTitle = styled.h2`
  margin-top: -200px;
  font-size: 24px;
  margin-bottom: 15px;
`;

const StartFormButton = styled.button`
  padding: 10px 20px;
  font-size: 16px;
  border: none;
  border-radius: 4px;
  background-color: #4285f4;
  color: #fff;
  cursor: pointer;
  transition: background-color 0.3s ease;

  &:hover {
    background-color: #1a73e8;
  }
`;

export { StartFormContainer, StartFormTitle, StartFormButton };
