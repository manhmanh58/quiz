import { QuizContext } from "contexts/quizContext";
import { useContext } from "react";
import { StartFormButton, StartFormContainer, StartFormTitle } from "./style";

const QuizStartForm = () => {
  const { handleStartQuiz } = useContext(QuizContext);
  const handleStart = () => {
    handleStartQuiz();
  };

  return (
    <StartFormContainer>
      <StartFormTitle>Welcome to the Quiz!</StartFormTitle>
      <p>Are you ready?</p>

      <StartFormButton onClick={handleStart}>Start</StartFormButton>
    </StartFormContainer>
  );
};

export default QuizStartForm;
