import { QuizContext } from "contexts/quizContext";
import { useContext, useEffect, useState } from "react";
import {
  FinishButton,
  FormContainer,
  NextButton,
  OptionButton,
  OptionItem,
  OptionList,
  QuestionCounter,
  Wrapper,
} from "./style";

const Question = () => {
  const { questions, currentQuestion, handleAnswer, handleNextQuestion } =
    useContext(QuizContext);
  const [shuffledOptions, setShuffledOptions] = useState([]);
  const [selectedAnswer, setSelectedAnswer] = useState("");

  const shuffleOptions = () => {
    const question = questions?.[currentQuestion];
    if (question) {
      const options = [...question?.incorrect_answers, question?.correct_answer];
      const shuffledOptions = shuffleArray(options);
      setShuffledOptions(shuffledOptions);
    }
  };

  const shuffleArray = (array) => {
    const shuffledArray = [...array];
    for (let i = shuffledArray.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [shuffledArray[i], shuffledArray[j]] = [
        shuffledArray[j],
        shuffledArray[i],
      ];
    }
    return shuffledArray;
  };

  const handleClick = (answer) => {
    setSelectedAnswer(answer);
  };

  const handleNext = () => {
    handleAnswer(selectedAnswer);
    handleNextQuestion();
    setSelectedAnswer("");
  };

  const handleFinish = () => {
    handleAnswer(selectedAnswer);
    setSelectedAnswer("");
  };
  useEffect(() => {
    shuffleOptions();
  }, [currentQuestion]);

  const question = questions?.[currentQuestion];
  const questionIndex = currentQuestion + 1;
  const totalQuestions = questions?.length;

  if (!question) {
    return null;
  }

  return (
    <Wrapper>
      <FormContainer>
        <QuestionCounter>
          Question {questionIndex}/{totalQuestions}
        </QuestionCounter>
        <h2>{question?.question}</h2>
        <OptionList>
          {shuffledOptions.map((option, index) => (
            <OptionItem key={index}>
              <OptionButton
                onClick={() => handleClick(option)}
                isSelected={selectedAnswer === option}
              >
                {option}
              </OptionButton>
            </OptionItem>
          ))}
        </OptionList>
        {currentQuestion < questions?.length - 1 ? (
          <NextButton disabled={selectedAnswer === ""} onClick={handleNext}>
            Next
          </NextButton>
        ) : (
          <FinishButton disabled={selectedAnswer === ""} onClick={handleFinish}>
            Finish
          </FinishButton>
        )}
      </FormContainer>
    </Wrapper>
  );
};

export default Question;
