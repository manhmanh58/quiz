import styled from "styled-components";

const Wrapper = styled.div`
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
  background: #f5f5f5;
`;

const FormContainer = styled.div`
  width: 400px;
  background: #fff;
  padding: 20px;
  border-radius: 8px;
`;

const QuestionCounter = styled.div`
  margin-bottom: 10px;
`;
const OptionList = styled.ul`
  list-style-type: none;
  padding: 0;
`;

const OptionItem = styled.li`
  margin-bottom: 10px;
`;

const OptionButton = styled.button`
  width: 100%;
  text-align: left;
  margin-top: 10px;
  padding: 10px 20px;
  font-size: 16px;
  border: 1px solid #ccc;
  border-radius: 4px;
  background: ${({ isSelected }) => (isSelected ? "#ccc" : "transparent")};
  color: ${({ isSelected }) => (isSelected ? "#fff" : "#000")};
  cursor: ${({ isSelected }) => (isSelected ? "default" : "pointer")};

  &:hover {
    background: ${({ isSelected }) => (isSelected ? "#ccc" : "#f5f5f5")};
  }
`;
const NextButton = styled.button`
  padding: 10px 20px;
  font-size: 16px;
  border: none;
  border-radius: 4px;
  background-color: #4285f4;
  color: #fff;
  cursor: ${({ disabled }) => (disabled ? "not-allowed" : "pointer")};
  opacity: ${({ disabled }) => (disabled ? 0.5 : 1)};
  transition: opacity 0.3s ease;

  &:hover {
    background-color: ${({ disabled }) => (disabled ? "#4285f4" : "#1a73e8")};
  }
`;

const FinishButton = styled(NextButton)`
  background-color: #34a853;

  &:hover {
    background-color: ${({ disabled }) => (disabled ? "#34a853" : "#1e7c32")};
  }
`;

export {
  Wrapper,
  FormContainer,
  QuestionCounter,
  OptionList,
  OptionItem,
  OptionButton,
  FinishButton,
  NextButton,
};
