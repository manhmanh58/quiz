import { QuizContext } from "contexts/quizContext";
import { useContext } from "react";
import {
  FormButton,
  FormContainer,
  FormGroup,
  FormLabel,
  FormQuestion,
  FormText,
  FormTitle,
} from "./style";

const ReviewAnswers = () => {
  const { questions, userAnswers, handlePlayAgain } = useContext(QuizContext);

  return (
    <FormContainer>
      <FormTitle>Review Answers</FormTitle>
      {questions?.map((question, index) => (
        <FormGroup key={index}>
          <FormQuestion>Question {index + 1}:</FormQuestion>
          <FormText>{question?.question}</FormText>
          <FormLabel>Correct Answer:</FormLabel>
          <FormText>{question?.correct_answer}</FormText>
          <FormLabel>Your Answer:</FormLabel>
          <FormText>{userAnswers[index]}</FormText>
        </FormGroup>
      ))}
      <FormButton onClick={handlePlayAgain}>Play Again</FormButton>
    </FormContainer>
  );
};

export default ReviewAnswers;
