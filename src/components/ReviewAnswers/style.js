import styled from "styled-components";

const FormContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  background-color: #f9f9f9;
  padding: 20px;
  border-radius: 8px;
  margin: 20px;
`;

const FormTitle = styled.h2`
  font-size: 24px;
  margin-bottom: 20px;
`;

const FormGroup = styled.div`
  margin-bottom: 20px;
  text-align: left;
`;
const FormQuestion = styled.label`
  font-size: 20px;
  margin-bottom: 5px;
  font-weight: bold;
`;
const FormLabel = styled.label`
  font-size: 16px;
  margin-bottom: 5px;
  font-weight: bold;
`;

const FormText = styled.p`
  font-size: 16px;
  margin: 8px 0px;
`;

const FormButton = styled.button`
  padding: 10px 20px;
  font-size: 16px;
  border: none;
  border-radius: 4px;
  background-color: #4285f4;
  color: #fff;
  cursor: pointer;
  transition: background-color 0.3s ease;

  &:hover {
    background-color: #1a73e8;
  }
`;

export {
  FormContainer,
  FormTitle,
  FormGroup,
  FormQuestion,
  FormLabel,
  FormText,
  FormButton,
};
