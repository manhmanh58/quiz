import styled from "styled-components";

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100vh;
`;

const ResultForm = styled.form`
  display: flex;
  flex-direction: column;
  background-color: #f8f9fa;
  padding: 40px;
  border-radius: 5px;
  max-width: 600px;
  box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
`;

const ResultTitle = styled.h2`
  font-size: 24px;
  font-weight: bold;
  margin-bottom: 20px;
`;

const ResultItem = styled.div`
  display: flex;
  align-items: center;
`;

const ResultLabel = styled.span`
  font-weight: bold;
  width: 120px;
`;

const ResultValue = styled.span`
  font-weight: bold;
`;

const ResultActions = styled.div`
  display: flex;
  justify-content: center;
  margin-top: 30px;
`;

const PlayAgainButton = styled.button`
  padding: 10px 20px;
  margin-right: 10px;
  background-color: #007bff;
  color: #fff;
  border: none;
  border-radius: 4px;
  cursor: pointer;
  font-size: 14px;
`;

const ReviewAnswersButton = styled.button`
  padding: 10px 20px;
  background-color: #28a745;
  color: #fff;
  border: none;
  border-radius: 4px;
  cursor: pointer;
  font-size: 14px;
`;

export {
  Wrapper,
  ResultForm,
  ResultTitle,
  ResultItem,
  ResultLabel,
  ResultValue,
  ResultActions,
  PlayAgainButton,
  ReviewAnswersButton,
};
