import { QuizContext } from "contexts/quizContext";
import { useContext } from "react";
import {
  PlayAgainButton,
  ResultActions,
  ResultForm,
  ResultItem,
  ResultLabel,
  ResultTitle,
  ResultValue,
  ReviewAnswersButton,
  Wrapper,
} from "./style";

const Result = () => {
  const {
    correctAnswers,
    timeTaken,
    resultText,
    handlePlayAgain,
    handleReviewAnswers,
  } = useContext(QuizContext);

  return (
    <Wrapper>
      <ResultForm>
        <ResultTitle>Quiz Result</ResultTitle>
        <ResultItem>
          <ResultLabel>Time Taken:</ResultLabel>
          <ResultValue>{timeTaken}</ResultValue>
        </ResultItem>
        <ResultItem>
          <ResultLabel>Correct Answers:</ResultLabel>
          <ResultValue>{correctAnswers}</ResultValue>
        </ResultItem>
        <ResultItem>
          <ResultLabel>Result:</ResultLabel>
          <ResultValue>{resultText}</ResultValue>
        </ResultItem>
        <ResultActions>
          <PlayAgainButton onClick={handlePlayAgain}>
            Play Again
          </PlayAgainButton>
          <ReviewAnswersButton onClick={handleReviewAnswers}>
            Review Answers
          </ReviewAnswersButton>
        </ResultActions>
      </ResultForm>
    </Wrapper>
  );
};

export default Result;
