import styled from "styled-components";

const LoadingContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 100vh;
`;

const LoadingText = styled.p`
  font-size: 18px;
  color: #555;
`;
export { LoadingContainer, LoadingText };
