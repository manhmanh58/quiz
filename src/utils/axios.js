import axios from "axios";
import { API_URL } from "../constant/common";

const axiosClient = axios.create({
  baseURL: API_URL,
  headers: {
    "content-type": "application/json",
  },
});

export default axiosClient;
