import React, { useContext } from "react";
import { QuizContext } from "contexts/quizContext";
import QuizStartForm from "components/QuizStart";
import Question from "components/Question";
import Result from "components/Result";
import ReviewAnswers from "components/ReviewAnswers";

const Quiz = () => {
  const { isQuizStarted, showReviewAnswers, isQuizFinished } =
    useContext(QuizContext);
  return (
    <div>
      {!isQuizStarted && !showReviewAnswers && !isQuizFinished ? (
        <QuizStartForm />
      ) : isQuizStarted && !showReviewAnswers && !isQuizFinished ? (
        <Question />
      ) : showReviewAnswers && isQuizFinished ? (
        <ReviewAnswers />
      ) : (
        <Result />
      )}
    </div>
  );
};

export default Quiz;
