import axiosClient from "../utils/axios";

export const quizAPI = {
  getResults: (amount) => {
    const url = `/api.php?amount=${amount}`;
    return axiosClient.get(url);
  },
};
