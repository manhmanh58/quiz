Welcome to quiz 👋

🚀 Getting Started

Go to project folder and install dependencies:

1. npm install or yarn install

2. create env folder and copy code in env.example folder

3. start: npm start or yarn start
